# LAMP en Debian/Bullseye con Vagrant

En este guion se enumeran los pasos para aprovisionar un ambiente LAMP (Linux, Apache, MySQL y PHP) sobre una instancia de Debian, que corre sobre VirtualBox y Vagrant. Para esta versión del guion se está utilizando la versión 11 (Bullseye).

## Índice

[1. Aprovisionar máquina virtual con Debian](./docs/provision-the-machine.md)

[2. Agregar entradas en el «hosts file»](./docs/hosts-entries.md)

[3. Instalar paquetes para servidor LAMP](./docs/lamp-installation.md)

[4. Configurar los hosts virtuales en Apache](./docs/vhosts-configuration.md)

[5. Utilizar Laravel en el sitio LFTS](./docs/laravel-on-lfts.md)

[6. Instalar base de datos para Laravel](./docs/db-installation.md)
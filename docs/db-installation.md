[<Volver](../Readme.md)

# Instalar base de datos MySQL

Con la excepción de la edición del archivo _.env_, todo este guion se ejecuta desde la **máquina virtual** de Debian.

## Instalación de paquetes

Instalar los paquetes de MariaDB (_«fork»_ libre de MySQL).

    sudo apt-get install mariadb-server mariadb-client

Conectarse como superadministrador al CLI de MySQL.

    sudo mysql

## Creación de base de datos y usuario

Crear la base de datos y el usuario para Laravel (en entornos de producción **nunca** utilice una contraseña débil como _secret_).

    create database db_laravel;
    
    create user user_laravel identified by 'secret';

    grant all privileges on db_laravel.* to user_laravel;

    flush privileges;

    quit;

Probar las credenciales, conectándose a MySQL con el CLI.

    mysql -u user_laravel --password="secret"

Verificar que el usuario pueda visualizar la base de datos.

    show databases;

![DB Laravel](../images/db-laravel.png)

## Habilitar vistas y capa de autenticación de Laravel

En este ejemplo se utilizará el sitio _lfts.isw811.xyz_, previamente configurado, sobre el cual se habilitarán las vistas y capa de autenticación de Laravel.

### Instalar Node.js y NPM

Utilizar CURL para instalar el gestor de versiones de Node.

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

    source ~/.bashrc

Instalar la última versión estable de Node y NPM.

    nvm install --lts

### Instalar las vista de autenticación de Laravel

Desde la máquina virtual ubicándose en _/vagrant/sites/lfts.isw811.xyz_.

    cd /vagrant/sites/lfts.isw811.xyz

    composer require laravel/ui

    php artisan ui bootstrap

    php artisan ui bootstrap --auth

    npm install && npm run dev

    npm run dev

## Conectar el proyecto de Laravel con la base de datos

Desde la **máquina anfitriona**, editar el archivo _.env_, personalizando los parámetros de host y base de datos.

    APP_URL=http://lfts.isw811.xyz

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=db_laravel
    DB_USERNAME=user_laravel

Desde la **máquina virtual**, ejecutar las migraciones de Laravel, para crear las tablas de base de datos.

    cd /vagrant/sites/lfts.isw811.xyz

    php artisan migrate

Comprobar la funcionalidad del sitio registrando a un nuevo usuario, desde el enlace de [registro](http://lfts.isw811.xyz/register).

Por medio del CLI de MySQL se puede verificar el registro.

    mysql -u user_laravel --password="secret"

    use db_laravel;

    show tables;

    desc users;

    select name, email from users;

La salida del `select` quedaría así.

![Select users](../images/select-users.png)

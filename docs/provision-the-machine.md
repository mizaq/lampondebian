[<Volver](../Readme.md)

# Aprovisionar máquina virtual Debian

Con este guion se aprovisionará una máquina virtual con Debian, a la cual se le dará el nombre de host _webserver_.

## Prerrequisito

Para ejecutar este guion con éxito se requiere instalar previamente tanto VirtualBox, como Vagrant, a continuación los enlaces de descarga (la instalación queda fuera del alcance de esta guía).

- Virtualbox [[Descargar]](https://www.virtualbox.org/wiki/Downloads)
- Vagrant [[Descargar]](https://www.vagrantup.com/downloads)

## Crear Vagrantfile

Para el aprovisionamiento de las máquinas virtuales, Vagrant utiliza un archivo denominado _Vagrantfile_, en cual se registran las directivas de configuración de la máquina, así como las posibles instrucciones de aprovisionamiento.

Desde la máquina anfitriona, se crea un directorio de trabajo y se inicializa el _Vagrantfile_. Se recomienda crear el directorio _VMs_ en `~` si trabaja desde una máquina anfitriona con GNU/Linux o en `~\Desktop\` si trabaja con Windows.

    mkdir -p VMs/webserver/sites
    
    cd VMs/webserver

    vagrant init debian/bullseye64

# Modificar el Vagrantfile

Configurar la máquina virtual con una IP estática y una red privada; esto se consigue agregando la siguiente directiva en el _Vagrantfile_.

    config.vm.network "private_network", ip: "192.168.56.10"

Montar el directorio _sites_ en la máquina virtual, agregando la siguiente directriz en el _Vagrantfile_.

    config.vm.synced_folder "sites/", "/home/vagrant/sites", owner: "www-data", group: "www-data"

## Iniciar el aprovisionamiento

Para iniciar el aprovisionamiento basado en el _Vagrantfile_ debe ejecutar el comando:

    vagrant up

La primera vez que se ejecuta `vagrant up` se descargará la imagen de _GNU/Linux Debian_ o la indicada en el _Vagrantfile_, sin embargo, las veces posteriores simplemente iniciará la máquina virtual.

## Comando para interactuar con la máquina Vagrant

Reemplace _webserver_ por el nombre del directorio que contiene el _Vagrantfile_ de la máquina con la que desea interactuar.

Apagar la máquina virtual.

    cd VMs/webserver

    vagrant halt

Iniciar la máquina virtual.

    cd VMs/webserver

    vagrant up

Conectarse a la máquina virtual.

    cd VMs/webserver

    vagrant ssh
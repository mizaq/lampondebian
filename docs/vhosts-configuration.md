[<Volver](../Readme.md)

# Crear los archivos _conf_ para hosts virtuales

Los hosts virtuales se utilizan en Apache para conseguir hospedar en un mismo servidor web múltiples sitios.

## Crear la estructuras de directorios para sitios

Desde la **máquina anfitriona**, en la misma ubicación del archivo _Vagrantfile_ del _webserver_, se crea la siguiente estructura de archivos y directorios.

    mkdir -p sites/lfts.isw811.xyz/public
    
    mkdir -p sites/miblog.com
    
    touch sites/lfts.isw811.xyz/public/index.html

    touch sites/lfts.isw811.xyz/index.html

    echo "<h1>¡Soy el index temporal de LFTS!</h1>" > sites/lfts.isw811.xyz/public/index.html

    echo "<h1>¡Bienvenido a MiBlog.com!</h1>" > sites/miblog.com/index.html

## Habilitar el sitio "MiBlog.com"

En la **máquina anfitriona** crear la plantilla para _conf_ para el VHOST de _miblog.com_.

    touch miblog.com.conf

Editar el archivo y pagar el siguiente contenido:

    <VirtualHost *:80>
	ServerAdmin webmaster@miblog.com
	ServerName miblog.com

	# Indexes + Directory Root.
	DirectoryIndex index.php index.html
	DocumentRoot /home/vagrant/sites/miblog.com

	<Directory /home/vagrant/sites/miblog.com>
		DirectoryIndex index.php index.html
		AllowOverride All
		Require all granted
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/miblog.com.error.log
	LogLevel warn
	CustomLog ${APACHE_LOG_DIR}/miblog.com.access.log combined
    </VirtualHost>

Finalmente se habilita el sitio _«miblog.conf»_ (requiere que previamente se hayan instalado los módulos _vhost_alias_, _rewrite_ y _ssl_).

    sudo cp /vagrant/miblog.com.conf /etc/apache2/sites-available/

    sudo apache2ctl -t

    sudo a2ensite miblog.com.conf

    sudo systemctl reload apache2.service

Para verificar el sitio, desde la máquina anfitriona, consulte la siguiente URL [http://miblog.com](http://miblog.com).

## Habilitar el sitio temporal para "lfts.isw811.xyz"

En la **máquina anfitriona** crear la plantilla para _conf_ para el VHOST de _lfts.isw811.xyz_.

    touch lfts.isw811.xyz.conf

Editar el archivo y pagar el siguiente contenido:

    <VirtualHost *:80>
	ServerAdmin webmaster@lfts.isw811.xyz
	ServerName lfts.isw811.xyz

	# Indexes + Directory Root.
	DirectoryIndex index.php index.html
	DocumentRoot /home/vagrant/sites/lfts.isw811.xyz/public

	<Directory /home/vagrant/sites/lfts.isw811.xyz/public>
		DirectoryIndex index.php index.html
		AllowOverride All
		Require all granted
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/lfts.isw811.xyz.error.log
	LogLevel warn
	CustomLog ${APACHE_LOG_DIR}/lfts.isw811.xyz.access.log combined
    </VirtualHost>

Finalmente se habilita el sitio _«miblog.conf»_ (requiere que previamente se hayan instalado los módulos _vhost_alias_, _rewrite_ y _ssl_).

    sudo cp /vagrant/lfts.isw811.xyz.conf /etc/apache2/sites-available/

    sudo apache2ctl -t

    sudo a2ensite lfts.isw811.xyz.conf

    sudo systemctl reload apache2.service

Para verificar el sitio, desde la máquina anfitriona, consulte la siguiente URL [http://lfts.isw811.xyz](http://lfts.isw811.xyz).
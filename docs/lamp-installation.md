[<Volver](../Readme.md)

# Instalar LAMP (Linux, Apache, MySQL, PHP)

Todo este guion se ejecuta desde la **máquina virtual** de Debian.

## Instalación de paquetes

Actualizar la lista de paquetes disponibles.

    sudo apt-get update

Instalar Vim, cURL, Apache2, MySQL y PHP, con el siguiente comando:

    sudo apt-get install vim vim-nox \
    curl git apache2 mariadb-server mariadb-client \
    php7.4 php7.4-bcmath php7.4-curl php7.4-json \
    php7.4-mbstring php7.4-mysql php7.4-xml

Se habilitan los módulos _vhost_alias_, _rewrite_ y _ssl_ de Apache2.

    sudo a2enmod vhost_alias rewrite ssl
    sudo systemctl restart apache2

Configurar el parámetro _ServerName_ de Apache.

    echo "ServerName webserver" | sudo tee -a /etc/apache2/apache2.conf

## Cambiar el nombre de la máquina

El nombre predeterminado de la imagen _Debian/Bullseye_ de _Vagrant_ es el nombre de la distro. Se procederá a cambiarlo por el nombre _webserver_.

    sudo hostnamectl set-hostname webserver

    sudo sed -i -e 's/bullseye/webserver/g' /etc/hosts
[<Volver](../Readme.md)

## Agregar entradas en el «hosts file»

Las entradas en el archivo «hosts» permiten simular la resolución de un nombre de dominio.

En los sistemas operativos tipo Windows, el archivo se encuentra en la ruta `c:\Windows\System32\drivers\etc`; desde un `cmd` ejecutado como administrador, se puede editar de la siguiente manera:

    cd \
    cd Windows\System32\drivers\etc
    notepad hosts

En los sistemas operativos tipo Unix, el archivo se encuentra en la ruta `/etc/hosts`; desde `bash` se puede editar de la siguiente manera:

    sudo nano /etc/hosts

En el archivo `hosts` de la **máquina anfitriona**, se ingresan las siguientes entradas, reemplazando `192.168.58.10` por la IP que corresponda a la máquina virtual.

    192.168.58.10 webserver
    192.168.58.10 miblog.com    
    192.168.58.10 lfts.isw811.xyz

Desde la terminal de `bash` o desde un `cmd`, se comprueban las entradas del _«hosts file»_, haciendo ping a los nombres de dominio, así:

    ping webserver
    ping miblog.com
    ping lfts.isw811.xyz
[< Volver](../README.md)

# Instalación de Laravel

## Instalación vía composer

Para crear proyectos de Laravel vía composer es prerrequisito instalar [composer](https://getcomposer.org/download/), en la máquina virtual _Debian/Buster_ se pueden ejecutar los siguientes comandos.

Descargar el `composer-setup.php`, así

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

Verificar la integridad del archivo

    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

Generar el `composer.phar`, así

    php composer-setup.php

Eliminar el _composer-setup.php_

    rm composer-setup.php

Crear folder en _/opt_, así

    sudo mkdir /opt/composer

Mover el binario de _composer_ a _/opt/composer_

    sudo mv composer.phar /opt/composer

Cambiar el propietario

    sudo chown root:root /opt/composer/composer.phar

Crear el enlace simbólico, para llamar _composer_ desde cualquier ubicación

    sudo ln -s /opt/composer/composer.phar /usr/bin/composer

## Creación de proyecto nuevo con Composer

Para crear un nuevo proyecto con _Composer_, utilizamos alguno de los siguientes comandos

    composer create-project laravel/laravel example-app

Para crear un proyecto en con una versión específica

    composer create-project laravel/laravel:5.7 example-app-5.7

## Extracción de la plantilla de proyecto

Al utilizar Vagrant para entorno de desarrollo y pruebas, puede resultar util hacer el _crafteo_ (elaboración) de la plantilla en el ambiente GNU/Linux, para luego copiarlo a la máquina anfitriona.

Desde la máquina Vagrant

    cd ~

    composer create-project laravel/laravel lfts.isw811.xyz

    tar cvfz lfts.isw811.xyz.tar.gz lfts.isw811.xyz

Desde `bash` _(Git Bash Here —en Windows—)_ en la **máquina anfitriona**, estando ubicados en la carpeta donde se ubica el _Vagrantfile_ (en este ejemplo `~/VMs/Webserver/`)

    cd ~/VMs/Webserver/

    eval "$(ssh-agent -s)"

    ssh-add .vagrant/machines/default/virtualbox/private_key

    scp vagrant@ulises:~/lfts.isw811.xyz.tar.gz .

    tar xvfz lfts.isw811.xyz.tar.gz

    mv lfts.isw811.xyz ./sites



    
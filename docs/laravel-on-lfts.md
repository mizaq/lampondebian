[<Volver](../Readme.md)

# Usando Laravel

En este guion se sobreescribirá el contenido del sitio `/vagrant/sites/lfts.isw811.xyz`, por una copia de la plantilla del Framework del Laravel en su versión 8.6.12.

## Instalar Composer

Para utilizar Laravel y gestionar dependencias de PHP, se utilizará _Composer_, el cuál se instala con los siguientes pasos, con el cual se podrá instalar cualquier paquete del repositorio [Packagist](https://packagist.org/).

Descargar el generador del binario.

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

Generar el binario.

    php composer-setup.php

Crear directorio en `opt` para gestionarlo como paquete opcional.

    sudo mkdir -p /opt/composer

Mover el binario y crear un enlance simbólico

    sudo mv composer.phar /opt/composer

Crear el enlace simbólico para utilizar _Composer_ desde cualquier ubicación.

    sudo ln -s /opt/composer/composer.phar /usr/bin/composer

## Reemplazar lfts.isw811.xyz con Laravel

Primero se removerá el sitio temporal.

    cd /vagrant/sites

    rm -rf lfts.isw811.xyz

Se utiliza _Composer_ para crear un nuevo proyecto de Laravel, en el sitio _lfts.isw811.xyz_.

    composer create-project laravel/laravel:8.6.12 lfts.isw811.xyz

Para verificar que el sitio ahora utiliza Laravel, desde la máquina anfitriona, consulte la siguiente URL [http://lfts.isw811.xyz](http://lfts.isw811.xyz).

## Crear plantilla reutilizable

Cada vez que se aprovisiona un nuevo sitio con Laravel se descargan todas sus dependencias, si lo desea puede guardar una copia comprimida del framework localmente, para reutilizarlo las veces que los desee.

    composer create-project laravel/laravel:8.6.12 plantilla-laravel-8
    
    tar cvfz plantilla-laravel-8.tar.gz plantilla-laravel-8

    rm -rf plantilla-laravel-8

Para reutilizar la plantilla, se ejecutan los siguientes comandos, reemplazando _mynewsite.com_ por el nombre del sitio deseado, ubicándose en el directorio donde se generó el archivo _.tar.gz_ de la plantilla.

    mkdir mynewsite.com

    tar xvfz plantilla-laravel-8.tar.gz --strip 1 -C mynewsite.com